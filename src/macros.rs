pub use crate::rational;
pub use crate::rational::*;
pub use typenum::{consts::*, Integer, Unsigned};

#[macro_export]
macro_rules! new_unit_system {
    ( $name:ident, $( $x:ident ),* ) => {
        mashup! {
            $(
                m["prefix" $x] = _ $x;
                m["left" $x] = Left_ $x;
                m["right" $x] = Right_ $x;
            )*
        }
        m! {
            #[derive(PartialEq, PartialOrd, Clone, Copy, Debug, Default)]
            pub struct $name <$($x: rational::GetND,)*> {
                raw: f64,
                $( "prefix" $x: $x, )*
            }
            impl <$($x: rational::GetND,)*> $name <$($x,)*> {
                #[allow(dead_code)]
                #[inline]
                pub fn new(raw: f64) -> Self {
                    Self {
                        raw: raw,
                        ..Default::default()
                    }
                }
            }
            impl <$($x: rational::GetND,)*> core::ops::Add for $name <$($x,)*> {
                type Output = Self;
                #[inline]
                fn add(self, rhs: Self) -> Self {
                    Self {
                        raw: self.raw + rhs.raw,
                        ..Default::default()
                    }
                }
            }
            impl <$($x: rational::GetND,)*> core::ops::Sub for $name <$($x,)*> {
                type Output = Self;
                #[inline]
                fn sub(self, rhs: Self) -> Self {
                    Self {
                        raw: self.raw - rhs.raw,
                        ..Default::default()
                    }
                }
            }
            impl<
                $( "left" $x: rational::GetND + core::ops::Add<"right" $x>, )*
                $( "right" $x: rational::GetND, )*
                > core::ops::Mul<$name<$( "right" $x, )*>>
                for $name<$( "left" $x, )*>
                where
                $( <"left" $x as core::ops::Add<"right" $x>>::Output: rational::GetND, )*
            {
                type Output = $name<
                        $(typenum::Sum<"left" $x, "right" $x>,)*
                    >;
                #[inline]
                fn mul(self, rhs: $name<$( "right" $x, )*>) -> Self::Output {
                    Self::Output {
                        raw: self.raw * rhs.raw,
                        ..Default::default()
                    }
                }
            }
            impl<
                $( "left" $x: rational::GetND + core::ops::Sub<"right" $x>, )*
                $( "right" $x: rational::GetND, )*
                > core::ops::Div<$name<$( "right" $x, )*>>
                for $name<$( "left" $x, )*>
                where
                $( <"left" $x as core::ops::Sub<"right" $x>>::Output: rational::GetND, )*
            {
                type Output = $name<
                        $(typenum::Diff<"left" $x, "right" $x>,)*
                    >;
                #[inline]
                fn div(self, rhs: $name<$( "right" $x, )*>) -> Self::Output {
                    Self::Output {
                        raw: self.raw / rhs.raw,
                        ..Default::default()
                    }
                }
            }
            impl <$($x: rational::GetND,)*> $name <$($x,)*> {
                #[allow(dead_code)]
                #[inline]
                pub fn powr<Exp: rational::GetND $(+ core::ops::Mul<$x> )*> (
                    &self,
                ) -> $name< $(typenum::Prod<Exp, $x>,)* >
                where
                $(<Exp as core::ops::Mul<$x>>::Output: rational::GetND,)*
                {
                    $name {
                        raw: self
                            .raw
                            .powf(Exp::Numer::to_i32() as f64 / Exp::Denom::to_i32() as f64),
                        ..Default::default()
                    }
                }
                #[allow(dead_code)]
                #[inline]
                pub fn pow<Numer: Integer + Default, Denom: Unsigned + Default> (
                    &self,
                ) -> $name< $(typenum::Prod<Rational::<Numer,Denom>, $x>,)* >
                where
                $(Rational::<Numer,Denom>: core::ops::Mul<$x>,)*
                $(<Rational::<Numer,Denom> as core::ops::Mul<$x>>::Output: rational::GetND,)*
                {
                    $name {
                        raw: self
                            .raw
                            .powf(Numer::to_i32() as f64 / Denom::to_i32() as f64),
                        ..Default::default()
                    }
                }
            }
            impl <$($x: rational::GetND,)*> core::ops::Mul<f64> for $name <$($x,)*> {
                type Output = Self;
                #[allow(dead_code)]
                #[inline]
                fn mul(self, rhs: f64) -> Self {
                    Self {
                        raw: self.raw * rhs,
                        ..Default::default()
                    }
                }
            }
            impl <$($x: rational::GetND,)*> core::ops::Mul<$name <$($x,)*>> for f64 {
                type Output = $name <$($x,)*>;
                #[allow(dead_code)]
                #[inline]
                fn mul(self, rhs: Self::Output) -> Self::Output {
                    Self::Output {
                        raw: self * rhs.raw,
                        ..Default::default()
                    }
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    // new_unit_system!(MyUS, Meter, Second, Kilogram, Radian);
    // #[test]
    // fn it_works() {
    //     let val = MyUS {
    //         raw: 12.3,
    //         _Meter: Rational::<P2, U4>::d(),
    //         _Second: Rational::<P3, U4>::d(),
    //         _Kilogram: Rational::<P2, U4>::d(),
    //         _Radian: Rational::<P3, U4>::d(),
    //     };
    //     assert_eq!(
    //         (val + val) * (val + val).powr::<Rational<P2, U1>>(),
    //         MyUS {
    //             raw: (12.3f64 + 12.3f64).powf(3.),
    //             _Meter: Rational::<P3, U2>::d(),
    //             _Second: Rational::<P9, U4>::d(),
    //             _Kilogram: Rational::<P3, U2>::d(),
    //             _Radian: Rational::<P9, U4>::d(),
    //         }
    //     );
    // }
}
