#![allow(unused_imports)]
#![allow(dead_code)]

use crate::rational::*;
use core::ops::{Add, Div, Mul, Sub};
use typenum::consts::*;
use typenum::{Diff, Exp, Gcd, Gcf, Integer, NonZero, PInt, PartialDiv, Prod, Sum, Unsigned};

#[derive(PartialEq, PartialOrd, Clone, Copy, Debug, Default)]
struct MyQuantity<Meter: GetND, Kilogram: GetND> {
    raw: f64,
    meter: Meter,
    kilogram: Kilogram,
}

impl<Meter: GetND, Kilogram: GetND> MyQuantity<Meter, Kilogram> {
    #[inline]
    fn powr<Exp: GetND + Mul<Meter> + Mul<Kilogram>>(
        &self,
    ) -> MyQuantity<Prod<Exp, Meter>, Prod<Exp, Kilogram>>
    where
        <Exp as Mul<Meter>>::Output: GetND,
        <Exp as Mul<Kilogram>>::Output: GetND,
    {
        MyQuantity {
            raw: self
                .raw
                .powf(Exp::Numer::to_i32() as f64 / Exp::Denom::to_i32() as f64),
            ..Default::default()
        }
    }
}

impl<ML: GetND + Add<MR>, KL: GetND + Add<KR>, MR: GetND, KR: GetND> Mul<MyQuantity<MR, KR>>
    for MyQuantity<ML, KL>
where
    <ML as Add<MR>>::Output: GetND,
    <KL as Add<KR>>::Output: GetND,
{
    type Output = MyQuantity<Sum<ML, MR>, Sum<KL, KR>>;
    fn mul(self, rhs: MyQuantity<MR, KR>) -> Self::Output {
        MyQuantity {
            raw: self.raw * rhs.raw,
            ..Default::default()
        }
    }
}

impl<ML: GetND + Sub<MR>, KL: GetND + Sub<KR>, MR: GetND, KR: GetND> Div<MyQuantity<MR, KR>>
    for MyQuantity<ML, KL>
where
    <ML as Sub<MR>>::Output: GetND,
    <KL as Sub<KR>>::Output: GetND,
{
    type Output = MyQuantity<Diff<ML, MR>, Diff<KL, KR>>;
    fn div(self, rhs: MyQuantity<MR, KR>) -> Self::Output {
        MyQuantity {
            raw: self.raw / rhs.raw,
            ..Default::default()
        }
    }
}

impl<Meter: GetND, Kilogram: GetND> Add<MyQuantity<Meter, Kilogram>>
    for MyQuantity<Meter, Kilogram>
{
    type Output = MyQuantity<Meter, Kilogram>;
    fn add(self, rhs: MyQuantity<Meter, Kilogram>) -> Self::Output {
        MyQuantity {
            raw: self.raw + rhs.raw,
            ..Default::default()
        }
    }
}

impl<Meter: GetND, Kilogram: GetND> Sub<MyQuantity<Meter, Kilogram>>
    for MyQuantity<Meter, Kilogram>
{
    type Output = MyQuantity<Meter, Kilogram>;
    fn sub(self, rhs: MyQuantity<Meter, Kilogram>) -> Self::Output {
        MyQuantity {
            raw: self.raw - rhs.raw,
            ..Default::default()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        let val = MyQuantity {
            raw: 12.3,
            meter: Rational::<P2, U4>::default(),
            kilogram: Rational::<P3, U4>::default(),
        };
        assert_eq!(
            (val + val) * (val + val).powr::<Rational<P2, U1>>(),
            MyQuantity {
                raw: (12.3f64 + 12.3f64).powf(3.),
                meter: Rational::<P3, U2>::default(),
                kilogram: Rational::<P9, U4>::default(),
            }
        );
    }
}
