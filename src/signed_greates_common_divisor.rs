use typenum::{
    consts::{U1, Z0},
    Gcd, Gcf, NInt, NonZero, PInt, Unsigned,
};
pub type Sgcfp<A, B> = <A as Sgcd<B>>::Pout;
pub type Sgcfu<A, B> = <A as Sgcd<B>>::Uout;
pub trait Sgcd<Rhs> {
    type Uout;
    type Pout;
}

impl<A1: Unsigned + Gcd<A2> + NonZero, A2: Unsigned> Sgcd<A2> for PInt<A1>
where
    <A1 as Gcd<A2>>::Output: Unsigned + NonZero,
{
    type Uout = Gcf<A1, A2>;
    type Pout = PInt<Gcf<A1, A2>>;
}

impl<A1: Unsigned + Gcd<A2> + NonZero, A2: Unsigned> Sgcd<A2> for NInt<A1>
where
    <A1 as Gcd<A2>>::Output: Unsigned + NonZero,
{
    type Uout = Gcf<A1, A2>;
    type Pout = PInt<Gcf<A1, A2>>;
}

impl<A2: Unsigned> Sgcd<A2> for Z0 {
    type Uout = U1;
    type Pout = PInt<U1>;
}
