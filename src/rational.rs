use crate::signed_greates_common_divisor::{Sgcd, Sgcfp, Sgcfu};
use core::ops::{Add, Div, Mul, Sub};
use typenum::{Integer, NonZero, PInt, Unsigned};

/// Only `struct Rational` is supposed to implement this trait
pub trait ShortFract {
    /// An equivalent `Rational`, with a Sgcd of 1 between Numer and Denom
    type Output;
}

/// Only `struct Rational` is supposed to implement this trait
pub trait GetND: Default + Mul {
    type Numer: Integer + Default;
    type Denom: Unsigned + NonZero + Default;
}

/// A compile-time rational number Numer/Denom
#[derive(Eq, PartialEq, Clone, Copy, Hash, Debug, Default)]
pub struct Rational<Numer: Integer + Default, Denom: Unsigned + Default> {
    _numer: Numer,
    _denom: Denom,
}

impl<Numer: Integer + Default, Denom: Unsigned + Default> Rational<Numer, Denom> {
    #[allow(dead_code)]
    pub fn d() -> Self {
        Self::default()
    }
}

impl<
        Numer: Integer + Default + Sgcd<Denom> + Div<<Numer as Sgcd<Denom>>::Pout>,
        Denom: Unsigned + Default + Div<<Numer as Sgcd<Denom>>::Uout>,
    > ShortFract for Rational<Numer, Denom>
where
    <Numer as Div<<Numer as Sgcd<Denom>>::Pout>>::Output: Integer + Default,
    <Denom as Div<<Numer as Sgcd<Denom>>::Uout>>::Output: Unsigned + Default,
{
    type Output = Rational<
        <Numer as Div<Sgcfp<Numer, Denom>>>::Output,
        <Denom as Div<Sgcfu<Numer, Denom>>>::Output,
    >;
}

impl<Numer: Integer + Default + Mul, Denom: Unsigned + NonZero + Default + Mul> GetND
    for Rational<Numer, Denom>
where
    <Numer as Mul>::Output: Integer
        + Default
        + Sgcd<<Denom as Mul>::Output>
        + Div<<<Numer as Mul>::Output as Sgcd<<Denom as Mul>::Output>>::Pout>,
    <Denom as Mul>::Output:
        Unsigned + Default + Div<<<Numer as Mul>::Output as Sgcd<<Denom as Mul>::Output>>::Uout>,
    <<Denom as Mul>::Output as Div<
        <<Numer as Mul>::Output as Sgcd<<Denom as Mul>::Output>>::Uout,
    >>::Output: Unsigned,
    <<Numer as Mul>::Output as Div<
        <<Numer as Mul>::Output as Sgcd<<Denom as Mul>::Output>>::Pout,
    >>::Output: Integer + Default,
{
    type Numer = Numer;
    type Denom = Denom;
}

impl<
        NL: Integer + Default + Mul<NR>,
        NR: Integer + Default,
        DL: Unsigned + Default + Mul<DR>,
        DR: Unsigned + Default,
    > Mul<Rational<NR, DR>> for Rational<NL, DL>
where
    <NL as Mul<NR>>::Output: Integer + Default,
    <DL as Mul<DR>>::Output: Unsigned + Default,
    <NL as Mul<NR>>::Output: Sgcd<<DL as Mul<DR>>::Output>,
    <DL as Mul<DR>>::Output: Div<<<NL as Mul<NR>>::Output as Sgcd<<DL as Mul<DR>>::Output>>::Uout>,
    <NL as Mul<NR>>::Output: Div<<<NL as Mul<NR>>::Output as Sgcd<<DL as Mul<DR>>::Output>>::Pout>,
    <<DL as Mul<DR>>::Output as Div<
        <<NL as Mul<NR>>::Output as Sgcd<<DL as Mul<DR>>::Output>>::Uout,
    >>::Output: Unsigned + Default,
    <<NL as Mul<NR>>::Output as Div<
        <<NL as Mul<NR>>::Output as Sgcd<<DL as Mul<DR>>::Output>>::Pout,
    >>::Output: Integer + Default,
{
    type Output =
        <Rational<<NL as Mul<NR>>::Output, <DL as Mul<DR>>::Output> as ShortFract>::Output;
    #[inline]
    fn mul(self, _: Rational<NR, DR>) -> Self::Output {
        Rational::default()
    }
}

impl<
        NL: Integer + Default + Mul<PInt<DR>>,
        NR: Integer + Default + Mul<PInt<DL>>,
        DL: Unsigned + Default + NonZero + Mul<DR>,
        DR: Unsigned + Default + NonZero + Mul<DR>,
    > Add<Rational<NR, DR>> for Rational<NL, DL>
where
    <DL as Mul<DR>>::Output: Unsigned + Default,
    <NL as Mul<PInt<DR>>>::Output: Add<<NR as Mul<PInt<DL>>>::Output>,
    <<NL as Mul<PInt<DR>>>::Output as Add<<NR as Mul<PInt<DL>>>::Output>>::Output:
        Integer + Default,
    <<NL as Mul<PInt<DR>>>::Output as Add<<NR as Mul<PInt<DL>>>::Output>>::Output:
        Sgcd<<DL as Mul<DR>>::Output>,
    <DL as Mul<DR>>::Output: Div<
        <<<NL as Mul<PInt<DR>>>::Output as Add<<NR as Mul<PInt<DL>>>::Output>>::Output as Sgcd<
            <DL as Mul<DR>>::Output,
        >>::Uout,
    >,
    <<NL as Mul<PInt<DR>>>::Output as Add<<NR as Mul<PInt<DL>>>::Output>>::Output: Div<
        <<<NL as Mul<PInt<DR>>>::Output as Add<<NR as Mul<PInt<DL>>>::Output>>::Output as Sgcd<
            <DL as Mul<DR>>::Output,
        >>::Pout,
    >,
    <<DL as Mul<DR>>::Output as Div<
        <<<NL as Mul<PInt<DR>>>::Output as Add<<NR as Mul<PInt<DL>>>::Output>>::Output as Sgcd<
            <DL as Mul<DR>>::Output,
        >>::Uout,
    >>::Output: Unsigned + Default,
    <<<NL as Mul<PInt<DR>>>::Output as Add<<NR as Mul<PInt<DL>>>::Output>>::Output as Div<
        <<<NL as Mul<PInt<DR>>>::Output as Add<<NR as Mul<PInt<DL>>>::Output>>::Output as Sgcd<
            <DL as Mul<DR>>::Output,
        >>::Pout,
    >>::Output: Integer + Default,
{
    // NL/DL + NR/DR =  (NL * DR + DL * NR) /DL * DR
    type Output = <Rational<
        <<NL as Mul<PInt<DR>>>::Output as Add<<NR as Mul<PInt<DL>>>::Output>>::Output,
        <DL as Mul<DR>>::Output,
    > as ShortFract>::Output;
    #[inline]
    fn add(self, _: Rational<NR, DR>) -> Self::Output {
        Rational::default()
    }
}

impl<
        NL: Integer + Default + Mul<PInt<DR>>,
        NR: Integer + Default + Mul<PInt<DL>>,
        DL: Unsigned + Default + NonZero + Mul<DR>,
        DR: Unsigned + Default + NonZero + Mul<DR>,
    > Sub<Rational<NR, DR>> for Rational<NL, DL>
where
    <DL as Mul<DR>>::Output: Unsigned + Default,
    <NL as Mul<PInt<DR>>>::Output: Sub<<NR as Mul<PInt<DL>>>::Output>,
    <<NL as Mul<PInt<DR>>>::Output as Sub<<NR as Mul<PInt<DL>>>::Output>>::Output:
        Integer + Default,
    <<NL as Mul<PInt<DR>>>::Output as Sub<<NR as Mul<PInt<DL>>>::Output>>::Output:
        Sgcd<<DL as Mul<DR>>::Output>,
    <DL as Mul<DR>>::Output: Div<
        <<<NL as Mul<PInt<DR>>>::Output as Sub<<NR as Mul<PInt<DL>>>::Output>>::Output as Sgcd<
            <DL as Mul<DR>>::Output,
        >>::Uout,
    >,
    <<NL as Mul<PInt<DR>>>::Output as Sub<<NR as Mul<PInt<DL>>>::Output>>::Output: Div<
        <<<NL as Mul<PInt<DR>>>::Output as Sub<<NR as Mul<PInt<DL>>>::Output>>::Output as Sgcd<
            <DL as Mul<DR>>::Output,
        >>::Pout,
    >,
    <<DL as Mul<DR>>::Output as Div<
        <<<NL as Mul<PInt<DR>>>::Output as Sub<<NR as Mul<PInt<DL>>>::Output>>::Output as Sgcd<
            <DL as Mul<DR>>::Output,
        >>::Uout,
    >>::Output: Unsigned + Default,
    <<<NL as Mul<PInt<DR>>>::Output as Sub<<NR as Mul<PInt<DL>>>::Output>>::Output as Div<
        <<<NL as Mul<PInt<DR>>>::Output as Sub<<NR as Mul<PInt<DL>>>::Output>>::Output as Sgcd<
            <DL as Mul<DR>>::Output,
        >>::Pout,
    >>::Output: Integer + Default,
{
    // NL/DL - NR/DR =  (NL * DR - DL * NR) /DL * DR
    type Output = <Rational<
        <<NL as Mul<PInt<DR>>>::Output as Sub<<NR as Mul<PInt<DL>>>::Output>>::Output,
        <DL as Mul<DR>>::Output,
    > as ShortFract>::Output;
    #[inline]
    fn sub(self, _: Rational<NR, DR>) -> Self::Output {
        Rational::default()
    }
}
