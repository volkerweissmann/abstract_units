#![recursion_limit = "2048"] // I'm sorry
pub extern crate typenum;
#[allow(unused_imports)]
#[macro_use]
extern crate mashup;
pub use mashup::*;
mod macros;
mod prototype;
pub mod rational;
mod signed_greates_common_divisor;

pub use macros::*;
